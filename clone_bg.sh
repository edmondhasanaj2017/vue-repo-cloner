#!/bin/bash

# Very important to switch to this folder before going further
cd $(dirname $0)

# Exit if any command fails for some reason
set -e

# Add config file
. ./clone_config.conf

# Start SSH Agent
eval `ssh-agent`
ssh-add $private_key

# Remove any possible artifacts directory that may be here
rm -rf artifacts 2> /dev/null
mkdir artifacts && cd artifacts

# Clone the repository in the artifacts folder
echo Cloning Repository...
git clone -b $branch $repo_url

# Go into ./artifacts/cloned_repo
cd *  

# Move the json config
echo Copying Config...
cp ../../$optional_config_file $optional_config_file_mv 

# Build it via the choice of the user
echo Installing all the dependencies
npm install

echo Building project...
npm run build

# Move the output to the public repository
echo Moving Build to Public...
rm -rf $build_path/* || mkdir $build_path
cp -r dist/* $build_path

# Cleanup
cd ../../
echo Cleanup...
rm -rf ./artifacts

echo "Done"
